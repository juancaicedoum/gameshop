# GameShop - Karma.

*Se realiza un Ecommerce donde se pone en venta video juegos.*

## Comenzando 🚀

_Para iniciar con el proyecto se descargó una plantilla desarrollada con Bootstrap, para obtener un diseño responsive incluido y poder realizarle modificaciones al código de la plantilla._

Mira **Deployment** para conocer como desplegar el proyecto.


### Descarga de la plantilla 📋

_Ingresar a la siguiente página, selecciona la plantilla que más te guste y descargala._
_(Algunas son gratuitas y otras tienen costo)._

```
https://themewagon.com/themes/free-reponsive-bootstrap-4-html5-ecommerce-website-template-karma/
```

## Construido con 🛠️

_Herramientas utilizadas para crear el proyecto:_

* [Bootstrap](https://getbootstrap.com/) - El conjunto de herramientas de código abierto de front-end más popular del mundo.
* [Bootstrap Templates & Themes](https://startbootstrap.com/themes) - Temas de Bootstrap gratuitos que están listos para personalizar y publicar. 
* [Bootsnipp](https://bootsnipp.com/) - Elementos de diseño y fragmentos de código para el marco Bootstrap HTML/CSS/JS.
* [themewagon](https://themewagon.com/themes/free-reponsive-bootstrap-4-html5-ecommerce-website-template-karma/) - Plantillas de código diseñadas para el marco Bootstrap HTML/CSS/JS.

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Juan Manuel Caicedo Castaño** - [juancaicedoum](https://github.com/juancaicedoum)
* **Jonatan Villa Castaño** - [JONATANVILLAC](https://github.com/JONATANVILLAC)

## ¿Como puedes ayudarnos? 🎁

* Comenta a otros sobre este proyecto y mi perfil. 📢
* Invitarme a una cerveza 🍺 acompañante perfecto para una reunión de negocios.
* Da las gracias públicamente 🤓.


---
⌨️ Proyecto realizado en el marco de la clase de Programación IV ❤️ por [juancaicedoum](https://github.com/juancaicedoum) & [JONATANVILLAC](https://github.com/JONATANVILLAC) 😊
